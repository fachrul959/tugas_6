-- Jumlah Komplain Tiap Bulan --
select count(`Complaint ID`) as "Jumlah Komplain",
       MONTHNAME(`Date Received`) as "Bulan",
       YEAR(`Date Received`) as "Tahun"
from consumercomplaints
group by YEAR(`Date Received`), MONTHNAME(`Date Received`);

-- Komplain yang memiliki tags ‘Older American’ --
select *
from consumercomplaints
where `Tags`='Older American';

-- Membuat View Jumlah Company Response to Customer --
create view jmlh_company_resp_to_cust as
select Company,
       sum(IF(`Company Response to Consumer` = 'Closed', 1, 0))                          as Closed,
       sum(IF(`Company Response to Consumer` = 'Closed with explanation', 1, 0))         as 'Closed with explanation',
       sum(IF(`Company Response to Consumer` = 'Closed with non-monetary relief', 1, 0)) as 'Closed with non-monetary relief'
from consumercomplaints
group by Company;